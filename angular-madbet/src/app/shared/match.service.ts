import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn:'root'
})

export class MatchService{
    private url = "https://madbet-staging.herokuapp.com/api/matchs"

    constructor(private http: HttpClient) {
    }

    getMatches(): Observable<any>{
        return this.http.get(this.url);
    }

    addMatch(match: Object): Observable<Object>{
        return this.http.post(this.url, match)
    }

    deleteMatch(id: number): Observable<any>{
        return this.http.delete(`${this.url}/${id}`, {responseType: 'text'})
    }
}