export class Match{
    _id?: number;
    idTeamH: number;
    idTeamA: number;
    idChampionship: number;
    score: {
        scoreH: number,
        scoreA: number
    };
    date = Date;
}